package com.mycompany.service;

import java.util.List;
import com.mycompany.mavenproject.model.Message;
import java.util.ArrayList;

public class ServiceClass {
        
    public List <Message> getMessage()
    {
        Message m1 = new Message(1,"My first REST","Shubham");
        Message m2 = new Message(2,"My second REST","Vaishnavi");
        List<Message> listObj = new ArrayList<>();
        listObj.add(m1);
        listObj.add(m2);
        return listObj;                                                          /*Sends List of object to messageResource class*/
    }
    
}
