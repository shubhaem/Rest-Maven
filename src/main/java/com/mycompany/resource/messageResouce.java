package com.mycompany.resource;
import com.mycompany.mavenproject.model.Message;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.mycompany.service.ServiceClass;
import java.util.List;

@Path("/messages")                              /* The following class is called when /messages URI is accessed*/
public class messageResouce {
    
    ServiceClass service = new ServiceClass();
    
    @GET                                        /*This annotation states that when there is a GET request from user the following method should be called*/
    @Produces(MediaType.APPLICATION_XML)        /* Response is of XML type,also is used to return user preffered type */
    public List<Message> retriveMessages()      
    {
        return service.getMessage();             /*This is the response of the GET request*/
    }
}
