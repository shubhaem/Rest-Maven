package com.mycompany.mavenproject.model;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement   
/*this helps jaxp to convert class elements into xml*/
public class Message {
    
    /* This class contains user details. This data can be retrived from DATABASE but for now stored in a class*/    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    

    
    private int id;
    private String message;
    private Date created;
    private String owner;

    public Message()
    {
        //always use this when sending data in XML and JSON
    }
    
    public Message(int id, String message, String owner) {
        this.id = id;
        this.message = message;
        this.created = new Date();
        this.owner = owner;
    }
    
    
}
